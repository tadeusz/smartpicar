#ifndef ___WEBAPI__HPP_____
#define ___WEBAPI__HPP_____
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <any>
#include <atomic>
#include <functional>
#include <list>
#include <memory>
#include <regex>
#include <string>
#include <tuple>
#include <vector>
#include <any>

namespace pigcd {
namespace webapi {

extern std::string static_files_directory;
extern std::string server_host;
extern std::string server_port;

struct request_data_t {
    /**
     * @brief Http method - GET, PUT, POST, ...
     */
    std::string method;
    /**
     * @brief raw URL with %* characters
     */
    std::string url;
    /**
     * @brief Headers taken from the request. All lower case.
     */
    std::vector<std::pair<std::string, std::string>> headers;
    /**
     * @brief The request data. The client reads whole request and only then
     * creates request_data_t object.
     */
    std::vector<char> data;
    /**
     * @brief decode uriencoded string into regular string
     *
     * @param uriencoded Encoded string, like
     * "/ind%20e%20%C5%82%C4%85%C4%87%C5%BCx.=html"
     * @return std::string decoded string like "/ind e łąćżx.=html"
     */
    static std::string decode(const std::string &uriencoded);
    /**
     * @brief Get the path object
     *
     * @return std::string return the path part of the url. It is before '?'
     * character.
     */
    std::string get_path() const;
    /**
     * @brief Get the path array object that is constructed by splitting the
     * path by the '/' character.
     *
     * @return std::vector<std::string> the list of url path elements. For paht
     * /x/y it will be {"x","y"}
     */
    std::vector<std::string> get_path_array() const;
    /**
     * @brief Get the query object that contains the list of query attributes
     *
     * @return std::vector<std::pair<std::string, std::string>> the key,value
     * pairs of query arguments
     */
    std::vector<std::pair<std::string, std::string>> get_query() const;

    /**
     * @brief Additional data that can be stored by the filters or user in order to achieve different interesting functionalities.
     */
    std::any user_data;
};

/**
 * @brief The datatype that represents response. It conatins minimal set of
 * values. The body is provided as an istream so it is easy to read from file or
 * even standard input.
 */
struct response_data_t {
    /**
     * @brief Socket associated with the connection to the client. If socket is
     * invalid (less than 0), then it tells that the connection has been closed
     * and the request was sent.
     */
    std::shared_ptr<int> client_socket;
    /**
     * @brief The protocol version. Usualy HTTP/1.1
     */
    std::string protocol;
    /**
     * @brief Status code. 200 means OK.
     */
    int status;
    /**
     * @brief Status code description.
     */
    std::string status_comment;
    /**
     * @brief Response headers. Lower case.
     */
    std::vector<std::pair<std::string, std::string>> headers;
    /**
     * @brief Send the request in the form of ifstream.
     *     It allows for sending large files. This closes the connection after
     * send.
     *
     * @param body The ifstream that will deliver data
     * @return int The number of bytes written. If error, then -1.
     */
    int send(std::shared_ptr<std::istream> body);
    /**
     * @brief Send the response that is provided as a string.
     *
     * @param body The body contents
     * @return int  The number of bytes written. If error, then -1.
     */
    int send(std::string body);
};

/**
 * @brief the request handler type. The request handler should send the response
 * using response.send. If this is a filter, then it should not send the
 * response. It can modify request. This allows for filters that would alter the
 * request. For example auth methods.
 */
using on_request_f =
    std::function<void(pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &params)>;

/**
 * @brief The mappings description. It allows for multiple different handlers
 * for different request addresses and methods. Matchers are test in order. You
 * can match multiple handlers to the same match - in this case, the first match
 * that will send the data will be the one that closes handling response. This
 * allows for filters that would change requests before the actual handling
 * happens.
 */
struct api_mapping_t {
    /**
     * @brief The method matcher.
     */
    std::regex method;
    /**
     * @brief The method matcher string. This allows for better documentation.
     */
    std::string method_string;
    /**
     * @brief The address match.
     */
    std::regex match;
    /**
     * @brief The address match string that translates into regex. This is for
     * documentation.
     */
    std::string match_string;
    /**
     * @brief Function that will handle the request. If it will execute
     * response.send, then it will be the ordinary handler. If it will not send
     * reaponse, then it will be filter that can alter request and response.
     */
    on_request_f handler;
    /**
     * @brief The name of the handler. For documentation.
     */
    std::string name;
    /**
     * @brief Handler description. For documentation.
     */
    std::string description;
};

/**
 * @brief The HTTP server.
 */
class mini_http_c {
   protected:
    std::atomic<bool> running_server;
    std::vector<std::tuple<int, std::string, std::string>> listen_sockets;

   public:
    /**
     * @brief List of mappings for request adresses.
     */
    std::list<api_mapping_t> mappings;

    /**
     * @brief Add request mapping.
     *
     * @param method The http match string.
     * @param match The address (URL) match string.
     * @param handler The handler for the request. If the handler does not
     * execute response.send, then it will be filter.
     * @param name Name of the API endpoint/filter.
     * @param desciption The description of the endpoint/filter.
     */
    void on_request(std::string method, std::string match, on_request_f handler, std::string name = "", std::string desciption = "");
    /**
     * @brief Construct a new mini http c object that is a HTTP server.
     *
     * @param addr Selected address.
     * @param port Selected port. Provided as string. On some OSes it can be
     * provided as text.
     */
    mini_http_c(std::string addr, std::string port);

    /**
     * @brief Start the server.
     *
     * @param heart_beat Function that tells the server if it should finish or
     * not. It should return true while server is running, and false when server
     * should close.
     */
    void server(std::function<bool()> heart_beat);

    /**
     * @brief This will generate API documentation based on the mappings and
     * descriptions.
     *
     * @return std::string The API documentation.
     */
    std::string generate_apidoc();
    /**
     * @brief Destroy the mini http c object and close all connections.
     */
    virtual ~mini_http_c();
};

/**
 * @brief The method that will return static files. It uses
 * pigcd::webapi::static_files_directory as an information about location of
 * static files.
 *
 * @param request request from client.
 * @param response response that will be sent.
 */
void handle_static(request_data_t &request, response_data_t &response, const std::vector<std::string> &);
/**
 * @brief Splits the string into parts divided by the split_char
 *
 * @param input the string, for example "abc&xyz&bbb"
 * @param split_char the character, for example '&'
 * @return std::vector<std::string> array of substrings, for example
 * {"abc","xyz","bbb"}.
 */
std::vector<std::string> split(const std::string input, char split_char);

/**
 * @brief Converts base64 string to the binary data.
 *
 * @param s The encoded base64 string
 * @return std::vector<char> the binary data.
 */
std::vector<char> base64tobin(const std::string &s);

void serve_file(std::string path, response_data_t &response);

}  // namespace webapi
}  // namespace pigcd
#endif
