/**
 * @file gpio-x86_64.cpp
 * @author Tadeusz Puźniakowski
 * @brief Fake implementation of steps generation on the PC - allows for
 * checking if steps are generated correctly
 * @version 0.1
 * @date 2022-01-31
 *
 * @copyright Copyright (c) 2022
 *
 */
/*

        CNC Machine driver for executing g-code for Raspberry Pi
        Copyright (C) 2022  Tadeusz Puźniakowski

        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

#ifdef ARCH_x86_64

#include <iostream>

#include "gpio.hpp"
//#include <chrono>
#include <list>
#include <thread>


namespace pigcd {

namespace gpio {

int pin_states_fake[32];
std::list<std::vector<int64_t>> reported_fake_states;


auto& output = std::cout;

void init_in(const std::vector<int> pins, const std::vector<int> pullups) {
    output << "# init_in [";
    for (auto p : pins) {
        output << " " << p;
    }
    output << " ] [";
    for (auto p : pullups) {
        output << " " << p;
    }
    output << "]" << std::endl;
}
void init_out(const std::vector<int> pins) {
    output << "# init_out [";
    for (auto p : pins) {
        output << " " << p;
    }
    output << "]" << std::endl;
}

inline void print_pin_state(const std::vector<int> state, int64_t t) {
    if (reported_fake_states.size() < 100000) {
        std::vector<int64_t> tolist;
        tolist.reserve(48);
        for (unsigned i = 1, statex = pin_states_fake[1]; i < state.size(); ++i, statex = state[i]) {
            tolist.push_back(statex);
        }
        tolist.push_back(t);
        tolist.push_back((reported_fake_states.size()) ? ((long int)t - (long int)reported_fake_states.front().at(31)) : (long int)0);
        reported_fake_states.push_back(std::move(tolist));
    }
}
void set_states(const std::vector<int>& pins, const std::vector<char>& states) {
    for (unsigned i = 0; i < pins.size(); i++) {
        if ((states.at(i) == 0) || (states.at(i) == 1)) pin_states_fake[pins.at(i)] = states.at(i);
    }
}
void set_pin_state(const int pin, const char state) {}
char get_state(const int pin) {
    // output << "# get_state " << pin << "  <-- " << pin_states_fake[pin]
    //        << std::endl;
    return pin_states_fake[pin];
}

}  // namespace gpio
}  // namespace pigcd

#endif
