CPP=g++
CFLAGS_OPENCV := $(shell pkg-config --cflags opencv4)
LIBS_OPENCV := $(shell pkg-config --libs opencv4)
LIBS=$(LIBS_OPENCV) -fopenmp 
#CFLAGS=-O3 -std=c++17 -DARCH_x86_64 -fopenmp $(CFLAGS_OPENCV)
CFLAGS=-O3 -std=c++17 -DARCH_armv7l  -fopenmp $(CFLAGS_OPENCV)

all: gpio-x86_64.o gpio-armv7l.o main.o webapi.o
	$(CPP) $^ -o smartpicar $(LIBS)

gpio-x86_64.o: libs/gpio-x86_64.cpp
	$(CPP) -c $(CFLAGS) $^

webapi.o: libs/webapi.cpp
	$(CPP) -c $(CFLAGS) $^

gpio-armv7l.o: libs/gpio-armv7l.cpp
	$(CPP) -c $(CFLAGS) $^

main.o: main.cpp
	$(CPP) -c $(CFLAGS) main.cpp

clean:
	rm -f *.o
	