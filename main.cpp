#include "libs/tp_args.hpp"
#include "libs/gpio.hpp"
#include "libs/webapi.hpp"
#include <signal.h>
#include <iostream>
//#include <CV2/opencv.hpp>

/**
 * 
echo 24 >/sys/class/gpio/export
echo 25 >/sys/class/gpio/export
echo out >/sys/class/gpio/gpio24/direction
echo out >/sys/class/gpio/gpio25/direction
echo 0 >/sys/class/gpio/gpio24/value
echo 1 >/sys/class/gpio/gpio24/value;sleep 1;echo 0 >/sys/class/gpio/gpio24/value
echo 1 >/sys/class/gpio/gpio25/value;sleep 1;echo 0 >/sys/class/gpio/gpio25/value
 */

#include "libs/gpio.hpp"
#include "libs/tp_args.hpp"
#include <atomic>
#include <chrono>
#include <functional>
#include <iostream>
#include <memory>
#include <thread>
#include <opencv2/opencv.hpp>
// #include <opencv4/highgui.hpp>


inline void set_thread_realtime() {
    sched_param sch_params;
    sch_params.sched_priority = sched_get_priority_max(SCHED_RR);
    if (pthread_setschedparam(pthread_self(), SCHED_RR, &sch_params)) {
        static int already_warned = 0;
        if (already_warned == 0) {
            std::cerr << "Warning: Failed to set Thread scheduling : " << std::strerror(errno) << std::endl;
            already_warned++;
        }
    }
}



void pwm_thread(std::atomic<int> *throttle_p, int period, int pin, bool realtime)
{
    if (realtime)  set_thread_realtime();
    using namespace std::chrono_literals;
    std::atomic<int> &throttle = *throttle_p;
    auto period_ns = std::chrono::nanoseconds(period);
    auto throttle_ns = std::chrono::nanoseconds(throttle);
    auto now = std::chrono::high_resolution_clock::now();

    while (((throttle_ns = std::chrono::nanoseconds(throttle)) >= 0ns) && (throttle_ns <= period_ns)) {
        pigcd::gpio::set_pin_state(pin, 1);
        if (throttle_ns == period_ns) std::this_thread::sleep_until(now + period_ns);
        else {
            std::this_thread::sleep_until(now + period_ns - throttle_ns);
            pigcd::gpio::set_pin_state(pin, 0);
            std::this_thread::sleep_until(now + period_ns);
        }
        now = std::chrono::high_resolution_clock::now();
    }
    pigcd::gpio::set_pin_state(pin, 0);
    std::cout << "ending thread" << std::endl;
}


bool is_thread_working = true;
void on_ctrl_c_signal(int) {
    is_thread_working = 0;  // terminate motors thread
}



int start_server(std::array<std::atomic<int>,3> *throttle_p, int cap_index = 0) {
    auto &throttle = *throttle_p;
    using namespace pigcd;
    using namespace std::chrono_literals;
    signal(SIGINT, on_ctrl_c_signal);
    webapi::mini_http_c srv(webapi::server_host, "3000");

    cv::VideoCapture video_cap(cap_index);
    std::atomic<int> frame_index = 0;

    std::atomic<int> active_frame = 0;

    std::vector<cv::Mat> frames(120);

    throttle[2] = 0; // disable servo

    auto vid_t = std::thread([&video_cap, &frame_index, &frames, &active_frame, &throttle](){
        while(video_cap.isOpened()) {
            cv::Mat frame;
            video_cap >> frame;
            int next_f = (frame_index+1) % frames.size();
            frames[next_f] = frame.clone();
            frame_index = next_f;
            if (active_frame > 0) {
                active_frame --;
                if (active_frame == 0) {
                    throttle[2] = 0; // disable servo
                    throttle[0] = 0; // disable servo
                    throttle[1] = 0; // disable servo
                }
            }
        }
    });
    srv.on_request(
        "GET", "/api/status",
        [&srv, &throttle](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            std::stringstream ss;
            ss << "Throttles: [ " << throttle[0] << "/20000000 " << throttle[1] << "/20000000  (1000000<" << throttle[2] << "<1400000)/20000000]";
            response.send(ss.str());
        },
        "Servos status", "The throttle power of servos and other stuff. ");
    srv.on_request(
        "GET", "/api/speed/.*",
        [&srv, &throttle, &active_frame](pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            std::stringstream ss;
            auto path_array = request.get_path_array();
            if (path_array.size() == 3) {
                try {
                    double val = std::stof(path_array[2]);
                    if ((val >= 0) && (val <= 1.0)) {
                        throttle[0] = val*20000000.0;
                        throttle[1] = 0;
                    }
                    if ((val < 0) && (val >= -1.0)) {
                        throttle[0] = 0;
                        throttle[1] = -val*20000000.0;
                    }
                    response.send("{\"status\":\"ok\",\"value\":" + std::to_string(val) + "}");
                } catch (...) {
                    response.send("{\"status\":\"error\",\"msg\":\"value out of range\"}");
                }
            } else {
                response.send("{\"status\":\"error\",\"msg\":\"you should provide speed with value from range [-1.0,1.0]\"}");
            }
        },
        "Set speed", "positive and negative values from -1 to 1");
    srv.on_request(
        "GET", "/api/turn/.*",
        [&srv, &active_frame, &throttle](pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            std::stringstream ss;
            auto path_array = request.get_path_array();
            if (path_array.size() == 3) {
                try {
                    const double range =   200000;
                    const double zero_v = 1150000;

                    double val = std::stof(path_array[2]);
                    if ((val >= -1.0) && (val <= 1.0)) {
                        throttle[2] = (-val)*range + zero_v;
                    }
                    response.send("{\"status\":\"ok\",\"value\":" + std::to_string(val) + "}");
                } catch (...) {
                    response.send("{\"status\":\"error\",\"msg\":\"value out of range\"}");
                }
            } else {
                response.send("{\"status\":\"error\",\"msg\":\"you should provide speed with value from range [-1.0,1.0]\"}");
            }
        },
        "Set turn", "positive and negative values from -1 to 1");

    srv.on_request(
        "GET", "/api/cam.jpg.*",
        [&srv, &throttle, &video_cap,&active_frame,&frames, &frame_index](pigcd::webapi::request_data_t &request, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            response.headers.push_back({"content-type","image/jpg"});
            response.headers.push_back({"Cache-Control","no-cache"});
            cv::Mat frame = frames[frame_index];
            std::vector< uchar > buf;
                    active_frame = 40;
            try {
                int w = 160;
                int h = 120;
                auto query = request.get_query();
                for (auto [k,v] : query) {
                    try {
                        if (k == "w") w = std::stoi(v);
                        if (k == "h") h = std::stoi(v);
                    } catch (...) {}
                }
                  cv::resize(frame, frame, cv::Size(w, h), cv::INTER_NEAREST);
            cv::imencode(".jpg",frame, buf);
            response.send(std::string(buf.begin(), buf.end()));
            } catch (...) {
                std::cout << "sending image... ERROR" << std::endl;
                response.send("ERROR");
            }
        },
        "Image from camera", "Returns the image from camera ");

    // video_cap

    srv.on_request(
        "GET", "/apidoc",
        [&srv](pigcd::webapi::request_data_t &, pigcd::webapi::response_data_t &response, const std::vector<std::string> &) {
            response.send(srv.generate_apidoc());
        },
        "WebAPI Documentatino", "Generates the documentation page.");
    srv.on_request("GET", ".*", pigcd::webapi::handle_static, "Static contents", "Returns the static demo for the API");


    srv.server([]() { return !(is_thread_working == 0); });
    video_cap.release();
    vid_t.join();
    return EXIT_SUCCESS;
}


int main(int argc, char** argv)
{
    std::vector<int> pins = {24, 25, 18};
    std::array<std::atomic<int>,3> throttle = {0,0,1200000}; ///< 0 to 

    using namespace tp::args;
    auto help = arg(argc, argv, "help", false);
    auto throttle_param = arg(argc, argv, "t", 1200000);
    auto cap_index = arg(argc, argv, "cap", 0);
    
    throttle[2] = throttle_param;
    pigcd::gpio::init_out(pins);

    if (help) {
        std::cout << "HELP" << std::endl;
        args_info(std::cout);
    }
    auto t0_fwd = std::thread(pwm_thread, &(throttle[0]), 20000000, pins[0],false);
    auto t1_back = std::thread(pwm_thread, &(throttle[1]), 20000000, pins[1],false);
    auto t2_servo = std::thread(pwm_thread, &(throttle[2]), 2000000, pins[2],true);

    using namespace std::chrono_literals;

    start_server(&throttle,cap_index);
    for (auto &e: throttle) e = -1;

    std::vector<char> states = {0, 0,0};
    pigcd::gpio::set_states(pins,states);
    t0_fwd.join();
    t1_back.join();
    t2_servo.join();
    return 0;
}

